import { Article } from "./article"

export interface IArticleService{
  getArticles(): Promise<Article[]>
  removeArticle (id: number):Promise<object>
  modifyArticle (article: Article):Promise<object>
  // addArticle (article: Article):Promise<Article[]>
}