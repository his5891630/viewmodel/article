export class Article {
  id!: number;
  href: string;
  title: string;
  date: string;
  author: string;
  category: string;
  categoryLink: string;
  summary: string;

  constructor(
    href: string,
    title: string,
    date: string,
    author: string,
    category: string,
    categoryLink: string,
    summary: string
  ) {
    (this.href = href || ""),
      (this.title = title||""),
      (this.date =date ||""),
      (this.author = author||""),
      (this.category = category ||""),
      (this.categoryLink =categoryLink || ""),
      (this.summary =summary || "");
  }
}
